import logo from './logo.svg';
import './App.css';
import React, { Component } from "react";
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import AboutPage from "./pages/AboutPage";
import ArticlePage from "./pages/ArticlePage";
import ArticleListPage from "./pages/ArticleListPage";
import HomePage from './pages/HomePage';
import NavBar from "./NavBar";
import NotFoundPage from "./pages/NotFoundPage";

class App extends Component {
  render() {
    return (
        <Router>
          <NavBar/>
          <div className="App container font-sans text-center">
            <Switch>
              <Route path="/"
                     component={HomePage} exact/>
              <Route path="/about"
                     component={AboutPage}/>
              <Route path="/article/:name"
                     component={ArticlePage}/>
              <Route path="/articles-list"
                     component={ArticleListPage}/>
              <Route component={NotFoundPage}/>
            </Switch>
          </div>
        </Router>
    )
        ;
  }
}

export default App;
