import React from 'react';
import { Link } from "react-router-dom";
import { GrArticle } from "react-icons/gr";

const ArticlesList = ({articles}) => (
    <>
      {articles.map( (article, key) => (
          <Link key={key} to={`/article/${article.name}`}>
            <div className="divide-y divide-gray text-left w-6/12 h-30 mb-8 block ">
              <h3 className="uppercase"><GrArticle/>{article.title}</h3>
              <p>{article.content[0].substring( 0, 150 )}...</p>
            </div>
          </Link>
      ) )}
    </>
);

export default ArticlesList;
