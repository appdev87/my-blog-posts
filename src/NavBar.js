import React from 'react';
import { Link } from 'react-router-dom';

const NavBar = () => (
    <nav className="container text-center mt-4">
      <ul>
        <Link className="hover:underline hover:text-red-600 p-4" to="/">Home</Link>
        <Link className="hover:underline hover:text-red-600 p-4" to="/about">About</Link>
        <Link className="hover:underline hover:text-red-600 p-4" to="/articles-list">Articles</Link>
      </ul>
    </nav>
);

export default NavBar;
