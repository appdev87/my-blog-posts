import React from 'react';


const HomePage = () => (
    <>
      <h1 className="font-sans text-4xl m-8">Hello, welcome to my blog!</h1>
      <p className="leading-relaxed m-4 divide-y divide-green-400">Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean lacinia bibendum nulla sed consectetur. Lorem
        ipsum
        dolor sit amet, consectetur adipiscing elit. Praesent commodo cursus magna, vel scelerisque nisl consectetur et.
        Aenean lacinia bibendum nulla sed consectetur.</p>
      
      <p className="leading-relaxed m-4 divide-y divide-green-400">
        Curabitur blandit tempus porttitor. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
        ridiculus
        mus. Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Fusce dapibus, tellus ac cursus
        commodo,
        tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Integer posuere erat a ante venenatis
        dapibus
        posuere velit aliquet. Nullam id dolor id nibh ultricies vehicula ut id elit.
      </p>
      
      <p className="leading-relaxed m-4 divide-y divide-green-400">
        Praesent commodo cursus magna, vel scelerisque nisl consectetur et. Etiam porta sem malesuada magna mollis
        euismod.
        Maecenas sed diam eget risus varius blandit sit amet non magna. Nulla vitae elit libero, a pharetra augue.
        Nullam
        quis risus eget urna mollis ornare vel eu leo. Cras mattis consectetur purus sit amet fermentum. Aenean lacinia
        bibendum nulla sed consectetur.
      </p>
      
      <p className="leading-relaxed m-4 divide-y divide-green-400">
        Vivamus sagittis lacus vel augue laoreet rutrum faucibus dolor auctor. Aenean eu leo quam. Pellentesque ornare
        sem
        lacinia quam venenatis vestibulum. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur
        ridiculus
        mus. Cras mattis consectetur purus sit amet fermentum.
      </p>
    </>
)

export default HomePage;
