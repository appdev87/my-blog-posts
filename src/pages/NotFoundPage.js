import React from 'react';

const NotFoundPage = () => (
    <h1 className="text-center text-red-300 text-title">404: Page not found</h1>
)

export default NotFoundPage;
