import React from 'react';
import articleContent from './article-content';
import ArticlesList from "../components/ArticlesList";

const ArticleListPage = () => (
    <>
      <h1 className="font-sans text-4xl m-8">ArticlesList</h1>
      <ArticlesList articles={articleContent} />
      
    </>
)

export default ArticleListPage;
